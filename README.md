# Ecdosis tutorials for Mac OS
## Installation
1. Download the tutorials-macos.tar.gz file from http://bitbucket.org/chasharpur/tutorials-macos. Click on the "Downloads" menu item on the left to access the downloads page.
2. Click on the tutorials-macos.tar.gz file in the Downloads menu. This will unpack it to the Downloads folder inside your home folder.
3. Using the finder, drag the tutorials-macos folder to your home folder.
4. Open the terminal program from Applications/Utilities
5. Type the command: cd ~/tutorials-macos
6. Type the command: ./ecdosis-run-macos.sh
7. In your browser's URL bar type: http://localhost:8081/
8. To stop ecdosis type: ./ecdosis-stop.sh inside the tutorials-macos folder
